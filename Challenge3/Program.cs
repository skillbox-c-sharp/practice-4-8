﻿

namespace Challenge3;
class Program
{
    static void Main(string[] args)
    {
        int row = 10;
        int col = 30;
        int maxRuns = 100;

        bool[,] lifeMatrix = new bool[row, col];
        int runs = 0;

        Random rand = new Random();
        for (int i = 0; i < lifeMatrix.GetLength(0); i++)
        {
            for (int j = 0; j < lifeMatrix.GetLength(1); j++)
            {
                lifeMatrix[i, j] = Convert.ToBoolean(rand.Next(0, 2));

            }
        }

        ShowMatrix(lifeMatrix);

        Thread.Sleep(100);

        while (runs++ < maxRuns)
        {
            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < col; j++)
                {
                    int numOfAliveNeighbors = GetNeighbors(lifeMatrix, i, j);
                    if (lifeMatrix[i, j])
                    {
                        if (numOfAliveNeighbors > 1 && numOfAliveNeighbors < 4)
                        {
                            lifeMatrix[i, j] = true;
                        }
                        else
                        {
                            lifeMatrix[i, j] = false;
                        }
                    }
                    else
                    {
                        if (numOfAliveNeighbors == 3)
                        {
                            lifeMatrix[i, j] = true;
                        }
                    }
                }
            }

            ShowMatrix(lifeMatrix);

            Console.WriteLine();
            Console.WriteLine(runs);

            Thread.Sleep(100);
        }

        Console.ReadKey();
    }

    static void ShowMatrix(bool[,] matrix)
    {
        Console.Clear();

        for (int i = 0; i < matrix.GetLength(0); i++)
        {
            for (int j = 0; j < matrix.GetLength(1); j++)
            {
                Console.Write($"{(matrix[i, j] ? "x" : " ")} ");

            }
            Console.WriteLine();
        }
    }

    static int GetNeighbors(bool[,] matrix, int x, int y)
    {
        int NumOfAliveNeighbors = 0;

        for (int i = x - 1; i < x + 2; i++)
        {
            for (int j = y - 1; j < y + 2; j++)
            {
                if (!((i < 0 || j < 0) || (i >= matrix.GetLength(0) || j >= matrix.GetLength(1))) && i != j)
                {
                    if (matrix[i, j] == true) NumOfAliveNeighbors++;
                }
            }
        }
        return NumOfAliveNeighbors;
    }

}

