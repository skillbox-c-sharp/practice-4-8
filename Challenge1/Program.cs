﻿namespace Challenge1;
class Program
{
    static void Main(string[] args)
    {
        Console.Write("Введите количество строк в матрице: ");
        int row = int.Parse(Console.ReadLine()!);
        Console.Write("Введите количество столбцов в матрице: ");
        int col = int.Parse(Console.ReadLine()!);

        Random rand = new Random();

        int[,] matrix = new int[row, col];
        int sum = 0;

        for (int i = 0; i < row; i++)
        {
            for (int j = 0; j < col; j++)
            {
                matrix[i, j] = rand.Next(0, 10);
                sum += matrix[i, j];
                Console.Write($"{matrix[i, j]} ");
            }
            Console.WriteLine();
        }
        Console.WriteLine($"Сумма всех элементов матрицы: {sum}");

        Console.ReadKey();
    }
}

