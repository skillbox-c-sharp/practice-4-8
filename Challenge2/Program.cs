﻿namespace Challenge2;
class Program
{
    static void Main(string[] args)
    {
        Console.Write("Введите количество строк в матрице: ");
        int row = int.Parse(Console.ReadLine()!);
        Console.Write("Введите количество столбцов в матрице: ");
        int col = int.Parse(Console.ReadLine()!);

        Random rand = new Random();

        int[,] matrix1 = new int[row, col];
        int[,] matrix2 = new int[row, col];
        int[,] sumMatrix = new int[row, col];

        for (int i = 0; i < row; i++)
        {
            for (int j = 0; j < col; j++)
            {
                matrix1[i, j] = rand.Next(0, 10);
                matrix2[i, j] = rand.Next(0, 10);
                sumMatrix[i, j] = matrix1[i, j] + matrix2[i, j];
            }
        }

        printMatrix(matrix1);
        printMatrix(matrix2);
        printMatrix(sumMatrix);

        Console.ReadKey();
    }

    static void printMatrix(int[,] matrix)
    {
        for (int i = 0; i < matrix.GetLength(0); i++)
        {
            for (int j = 0; j < matrix.GetLength(1); j++)
            {
                Console.Write($"{matrix[i, j]} ");
            }
            Console.WriteLine();
        }
        Console.WriteLine();
    }
}

